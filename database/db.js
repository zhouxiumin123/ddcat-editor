const mongoose = require('mongoose');
const db = mongoose.connect('mongodb://root:yjs602a@mongo:27017/ddcat?authSource=admin', { useMongoClient: true });//；连接数据库

const con = mongoose.connection;
con.on('error', console.error.bind(console, '连接错误：'));
con.once('open', function(callback){
    console.log('MongoDB连接成功！！')
});

const Schema = mongoose.Schema;   //  创建模型
const userScheMa = new Schema({
    name: String,
    password: String
}); //  定义了一个新的模型，但是此模式还未和users集合有关联

const sitedScheMa = new Schema({
    title: {type:String,unique: true},
    guid: {type:String},
    intro: String,
    author: String,
    xml: String,
    updateTime: {
        type: Date,
        default: Date.now
    }
});

exports.user = db.model('users', userScheMa); //  与users集合关联
exports.sited = db.model('sited', sitedScheMa); //  与sited集合关联
