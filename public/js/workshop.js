/**
 * Created by zhouxiumin on 2018/6/1.
 */

$(document).ready(function(){
    var editor = CodeMirror.fromTextArea(document.getElementById("code"), {
        mode: "text/html",
        lineNumbers: true,
        matchBrackets: true,
        matchClosing: true,
        alignCDATA: true
    });
    editor.setSize('auto','800px');

    $('#save').click(function () {
        console.log('save');
        var xml = editor.getValue();
        var id = $('#xml_id').val();
        $.post('/ddcat/api/modify', {id:id,xml:xml},function (result) {
            $('#result').html(result);
        });
    })


});