/**
 * Created by zhouxiumin on 2018/6/1.
 */

$(document).ready(function(){
    var editor = CodeMirror.fromTextArea(document.getElementById("code"), {
        mode: "text/html",
        lineNumbers: true,
        matchBrackets: true,
        matchClosing: true,
        alignCDATA: true
    });
    editor.setSize('auto','800px');
});