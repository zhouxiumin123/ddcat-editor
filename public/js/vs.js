// videojs 简单使用
var player = videojs('my-player', {
    textTrackDisplay: false,
    posterImage: false,
    errorDisplay: true,
    playbackRates: [0.5, 1, 2, 4, 8, 16],
    notSupportedMessage: "不支持此视频文件",
    nativeControlsForTouch: true,
    breakpoints: {
        tiny: 300,
        xsmall: 400,
        small: 500,
        medium: 600,
        large: 700,
        xlarge: 800,
        huge: 900
    }
});

player.seekButtons({
    forward: 30,
    back: 10
});